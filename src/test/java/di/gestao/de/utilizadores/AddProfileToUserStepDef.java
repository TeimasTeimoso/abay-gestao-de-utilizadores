package di.gestao.de.utilizadores;

import com.google.gson.Gson;
import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.repositories.UserRepository;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AddProfileToUserStepDef {
    private static final Logger log = LoggerFactory.getLogger(SpringApp.class);

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    ResultActions action;

    User u;

    @Given("the service url with username={word} and profile={word}")
    public void givenUrladdUserProfile(String username, String profile) {
        this.u = new User(username, profile);
    }

    @And("the user exists")
    public void userToUpdateProfile() {
        Assert.assertTrue(userRepository.findByUsername(this.u.getUsername()) != null);
    }

    @And("the user does not exist")
    public void userToUpdateProfileDoesNotExist() {
        Assert.assertTrue(userRepository.findByUsername(this.u.getUsername()) == null);
    }

    @When("the client sends a PUT request to /addProfileToUser")
    public void putRequestUpdateProfile() throws Exception {
        action = mvc.perform(put("/addProfileToUser")
                .content(new Gson().toJson(this.u))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));
    }

    @Then("the client receives a 204 code.")
    public void addProfileWithSuccess() throws Exception {
        action.andExpect(status().isNoContent());
    }

    @Then("the client receives a code 404 and \"User not found\".")
    public void addProfileFailed() throws Exception {
        action.andExpect(status().isNotFound())
                .andExpect(content().string("User not found"));
    }
}
