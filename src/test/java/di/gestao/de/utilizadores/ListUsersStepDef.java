package di.gestao.de.utilizadores;

import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.repositories.UserRepository;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ListUsersStepDef {
    private static final Logger log = LoggerFactory.getLogger(SpringApp.class);

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    ResultActions action;

    User u;
    String field;
    String value;

    @Given("the service url")
    public void givenUrlListUsers() {
        String url = "";
    }

    @When("the client sends a GET request to /listUsers")
    public void getRequestToListusers() throws Exception {
        action=mvc.perform(get("/listUsers").contentType(MediaType.APPLICATION_JSON));
    }

    @Then("the client receives a response code 200")
    public void listUsers() throws Exception {
        action.andExpect(status().isOk());
    }

    @And("the client receives the list of users")
    public void receiveList() throws Exception {

        action.andExpect(jsonPath("$[*].username", containsInAnyOrder("rubenteimas", "pedrosalgueiro", "quentintarantino")))
                .andExpect(jsonPath("$[*].email", containsInAnyOrder("m47753@alunos.uevora.pt", "pds@uevora.pt", "qt@hollywood.com")))
                .andExpect(jsonPath("$[*].password", containsInAnyOrder("123", "456", "123")))
                .andExpect(jsonPath("$[*].profile", containsInAnyOrder("admin", "admin", "basic")));
    }
}
