package di.gestao.de.utilizadores;

import di.gestao.de.utilizadores.repositories.UserRepository;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SearchUserStepDef {
    private static final Logger log = LoggerFactory.getLogger(SpringApp.class);

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    ResultActions action;

    String username;

    @Given("the service url plus the username={word}")
    public void givenUrlSearchUsers(String username) {
        String url = "";
        this.username = username;
    }

    @And("the username does exist")
    public void userExistsSearchUser() {
        assertTrue(userRepository.findByUsername(this.username) != null);
    }

    @And("the username is non existent")
    public void userDoesNotExistsSearchUser() {
        assertTrue(userRepository.findByUsername(this.username) == null);
    }

    @When("the client sends a GET request to /searchUser")
    public void getRequestSearchUser() throws Exception {
        String endpoint = "/searchUser/"+this.username;
        action = mvc.perform(get(endpoint).contentType(MediaType.APPLICATION_JSON));
    }


    @Then("the client receives response code {int} and {}.")
    public void searchUserResult(Integer code, String empty) throws Exception {
        if (code == 200) {
            action.andExpect(status().isOk())
                .andExpect(jsonPath("username", Matchers.is("rubenteimas")))
                .andExpect(jsonPath("email", Matchers.is("m47753@alunos.uevora.pt")))
                .andExpect(jsonPath("password",Matchers.is("123")))
                .andExpect(jsonPath("profile",Matchers.is("basic")));
        }
        else if (code == 404) {
            action.andExpect(status().isNotFound())
                    .andExpect(MockMvcResultMatchers.content().string("User does not exist"));
        }
    }

}
