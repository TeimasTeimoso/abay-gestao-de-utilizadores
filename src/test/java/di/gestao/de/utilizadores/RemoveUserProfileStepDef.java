package di.gestao.de.utilizadores;

import com.google.gson.Gson;
import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.repositories.UserRepository;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RemoveUserProfileStepDef {
    private static final Logger log = LoggerFactory.getLogger(SpringApp.class);

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    ResultActions action;

    String username;

    @Given("the service url, username={word}")
    public void givenUrladdUserProfile(String username) {
        this.username = username;
    }

    @And("the target user exists")
    public void userToUpdateProfile() {
        Assert.assertTrue(userRepository.findByUsername(this.username) != null);
    }

    @And("the target user does not exist")
    public void userToUpdateProfileDoesNotExist() {
        Assert.assertTrue(userRepository.findByUsername(this.username) == null);
    }

    @When("the client sends a PUT request to /removeProfileFromUser")
    public void putRequestUpdateProfile() throws Exception {
        String endpoint = "/removeProfileFromUser/" + this.username;
        action = mvc.perform(put(endpoint).contentType(MediaType.APPLICATION_JSON));
    }

    @Then("client receives a 204 code.")
    public void removeProfileWithSuccess() throws Exception {
        action.andExpect(status().isNoContent());
    }

    @Then("client receives the code 404 and \"User does not exist\".")
    public void removeProfileFailed() throws Exception {
        action.andExpect(status().isNotFound())
                .andExpect(content().string("User does not exist"));
    }
}
