package di.gestao.de.utilizadores;

import com.google.gson.Gson;
import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.repositories.UserRepository;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

public class CreateUserStepDef extends StepDefinition {
    private static final Logger log = LoggerFactory.getLogger(SpringApp.class);

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    ResultActions action;

    User u;
    String field;
    String value;

    @Given("the service url and {word}={word}")
    public void givenUrl(String field, String value) {
        String url = "";

        this.field = field;
        this.value = value;
    }


    @When("the username and email do not exist in the database")
    public void usernameAndEmailDontExist() {
        assert(this.userRepository.findByUsername("quentintarantino") == null);
        assert(this.userRepository.findByEmail("qt@hollywood.com") == null);
    }

    @When("the {word} exists in the database")
    public void fieldExist(String field) {
        if (field.equals("username")) {
            assert(userRepository.findByUsername(this.value) != null);
        }
        else {
            assert(userRepository.findByEmail(this.value) != null);
        }
    }

    @And("the client sends a POST request to /createUser")
    public void postToCreateUser() throws Exception {

        if (this.field.equals("username")) {
            this.u = new User(this.value, "qt@hollywood.com", "123");
        }
        else {
            this.u = new User("pedropatinho", this.value, "123");
        }

        action = mvc.perform(post("/createUser")
                    .content(new Gson().toJson(this.u))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON));

    }


    @Then("the client receives a 201 response code.")
    public void createUser() throws Exception {
        action.andExpect(status().isCreated());
    }

    @Then("the client receives a response code 500 and {string}.")
    public void failToCreateUser(String response) throws Exception {
        action.andExpect(status().isInternalServerError())
            .andExpect(content().string(response));
    }
}
