package di.gestao.de.utilizadores;

import di.gestao.de.utilizadores.repositories.UserRepository;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RemoveUserStepDef {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    ResultActions action;

    String username;


    @Given("the service url and the username={word}")
    public void givenUrlRemoveUser(String word) {
        this.username = word;
    }

    @And("the username exists")
    public void userExistsToRemove() {
        Assert.assertTrue(userRepository.findByUsername(this.username) != null);
    }

    @And("the username does not exist in the database")
    public void userToRemoveDoesNotExist() {
        Assert.assertTrue(userRepository.findByUsername(this.username) == null);
    }

    @When("the client sends a DELETE request to /removeUser")
    public void deleteRequestRemoveUser() throws Exception {
        action = mvc.perform(delete("/removeUser/{username}", this.username));
    }

    @Then("the client receives a 204 response code.")
    public void removeUserWithSuccess() throws Exception {
        action.andExpect(status().isNoContent());
    }

    @Then("the client receives a response code 404 and \"User not found\".")
    public void cannotRemoveUser() throws Exception {
        action.andExpect(status().isNotFound())
            .andExpect(content().string("User not found"));
    }
}
