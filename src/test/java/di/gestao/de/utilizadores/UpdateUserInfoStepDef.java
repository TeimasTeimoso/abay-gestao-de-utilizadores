package di.gestao.de.utilizadores;

import com.google.gson.Gson;
import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.repositories.UserRepository;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UpdateUserInfoStepDef {
    private static final Logger log = LoggerFactory.getLogger(SpringApp.class);

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    ResultActions action;

    User u;
    String username;

    @Given("the service url with username being {word}")
    public void givenUrlUpdateInfo(String username) {
        String url = "";

        this.username = username;
        this.u = new User(username, "changed@email.com", null, null);
    }

    @And("the user for that username exists")
    public void updateUserExitst() {
        assertTrue(userRepository.findByUsername(this.username) != null);
    }

    @And("the user for that username does not exist")
    public void updateUserDoesnNotExist() {
        assertTrue(userRepository.findByUsername(this.username) == null);
    }

    @When("the client sends a PUT request to /updateUserInfo")
    public void putToUpdateUserInfo() throws Exception {
        action = mvc.perform(put("/updateUserInfo")
                .content(new Gson().toJson(this.u))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

    }

    @Then("the client receives 204 response code.")
    public void updateInfoSuccess() throws Exception {
        action.andExpect(status().isNoContent());
    }

    @Then("the client receives a response code 404, Not Found, and \"User does not exist\".")
    public void failToUpdateInfo() throws Exception {
        action.andExpect(status().isNotFound())
                .andExpect(content().string("User does not exist"));
    }

}
