Feature: Remove profile from User
  Remove profile from user on abay's platform

  Scenario: Profile removed with success
    Given the service url, username=rubenteimas
    And the target user exists
    When the client sends a PUT request to /removeProfileFromUser
    Then client receives a 204 code.

  Scenario: Removing profile from nonexistent user
    Given the service url, username=danielmendonca
    And the target user does not exist
    When the client sends a PUT request to /removeProfileFromUser
    Then client receives the code 404 and "User does not exist".
