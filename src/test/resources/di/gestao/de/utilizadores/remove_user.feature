Feature: Remove User
  Remove user on abay's platform

  Scenario: User removed with success
    Given the service url and the username=quentintarantino
    And the username exists
    When the client sends a DELETE request to /removeUser
    Then the client receives a 204 response code.

  Scenario: Removing nonexistent user
    Given the service url and the username=martinscorcese
    And the username does not exist in the database
    When the client sends a DELETE request to /removeUser
    Then the client receives a response code 404 and "User not found".
