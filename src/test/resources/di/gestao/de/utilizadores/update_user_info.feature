Feature: Update user information
  Update user information in abay's platform

  Scenario: Update user info for existent username and fields
    Given the service url with username being pedrosalgueiro
    And the user for that username exists
    When the client sends a PUT request to /updateUserInfo
    Then the client receives 204 response code.

  Scenario: Failure when updating user information
    Given the service url with username being denislapuste
    And the user for that username does not exist
    When the client sends a PUT request to /updateUserInfo
    Then the client receives a response code 404, Not Found, and "User does not exist".