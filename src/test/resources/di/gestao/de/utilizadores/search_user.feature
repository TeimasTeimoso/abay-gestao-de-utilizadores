Feature: Search user
  Search for the user in abay's platform

  Scenario: Search for existent user
    Given the service url plus the username=rubenteimas
    And the username does exist
    When the client sends a GET request to /searchUser
    Then the client receives response code 200 and the client receives the user information.

  Scenario: Search for nonexistent user
    Given the service url plus the username=bernadomarques
    And the username is non existent
    When the client sends a GET request to /searchUser
    Then the client receives response code 404 and "User does not exist".
