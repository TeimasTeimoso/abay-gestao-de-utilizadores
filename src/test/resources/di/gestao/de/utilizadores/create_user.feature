Feature: Create User
  Create user on abay's platform

  Scenario: Profile added with success
    Given the service url and username=quentintarantino
    When the username and email do not exist in the database
    And the client sends a POST request to /createUser
    Then the client receives a 201 response code.

  Scenario Outline: Failure when adding user
    Given the service url and <object>=<value>
    When the <object> exists in the database
    And the client sends a POST request to /createUser
    Then the client receives a response code 500 and <answer>.

    Examples:
      | object   | value         | answer                             |
      | username | rubenteimas   | "Username already taken"           |
      | email    | pds@uevora.pt | "Email already exists in database" |