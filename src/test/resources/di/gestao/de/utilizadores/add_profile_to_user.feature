Feature: Add profile to User
  Add profile to user on abay's platform

  Scenario: Profile added with success
    Given the service url with username=pedrosalgueiro and profile=admin
    And the user exists
    When the client sends a PUT request to /addProfileToUser
    Then the client receives a 204 code.

  Scenario: Failure when adding profile to user
    Given the service url with username=carlosparedes and profile=admin
    And the user does not exist
    When the client sends a PUT request to /addProfileToUser
    Then the client receives a code 404 and "User not found".