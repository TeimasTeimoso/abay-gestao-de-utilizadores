Feature: List all users
  List all the users for the abay's platform

  Scenario: List all users successufly
    Given the service url
    When the client sends a GET request to /listUsers
    Then the client receives a response code 200
    And the client receives the list of users
