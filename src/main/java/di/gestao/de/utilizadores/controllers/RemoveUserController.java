package di.gestao.de.utilizadores.controllers;

import di.gestao.de.utilizadores.services.RemoveUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RemoveUserController {

    @Autowired
    RemoveUserService service;

    @DeleteMapping("/removeUser/{username}")
    public ResponseEntity removeUser(@PathVariable String username) {
        Boolean res = service.RemoveUserService(username);

        if (res)
            return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT);

        return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
    }
}
