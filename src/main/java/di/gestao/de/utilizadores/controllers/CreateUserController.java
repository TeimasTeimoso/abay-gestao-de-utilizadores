package di.gestao.de.utilizadores.controllers;

import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.services.CreateUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreateUserController {

    @Autowired
    CreateUserService service;

    @PostMapping("/createUser")
    public ResponseEntity createUser(@RequestBody User user) {
        String res = service.createUser(user);

        if (res.equals("User created!"))
            return new ResponseEntity<HttpStatus>(HttpStatus.CREATED);

        return new ResponseEntity<String>(res, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
