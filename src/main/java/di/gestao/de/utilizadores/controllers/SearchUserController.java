package di.gestao.de.utilizadores.controllers;

import com.google.gson.Gson;
import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.services.SearchUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchUserController {

    @Autowired
    SearchUserService service;

    @GetMapping("/searchUser/{username}")
    public ResponseEntity searchUser(@PathVariable String username) {
        User user = service.getUserInformation(username);

        if(user != null)
            return new ResponseEntity<User>(user, HttpStatus.OK);

        return new ResponseEntity<String>("User does not exist", HttpStatus.NOT_FOUND);
    }
}
