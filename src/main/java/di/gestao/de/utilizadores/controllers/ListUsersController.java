package di.gestao.de.utilizadores.controllers;

import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.repositories.UserRepository;
import di.gestao.de.utilizadores.services.ListUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ListUsersController {

    @Autowired
    ListUsersService service;

    @GetMapping("listUsers")
    public List<User> listUsers() {
        return service.listUsers();
    }
}
