package di.gestao.de.utilizadores.controllers;

import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.services.UpdateUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UpdateUserInfoController {

    @Autowired
    UpdateUserInfoService service;

    @PutMapping("/updateUserInfo")
    public ResponseEntity updateUserInfo(@RequestBody User user) {
        Boolean res = service.updateUserInfo(user);

        if(res)
            return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT);

        return new ResponseEntity<String>("User does not exist", HttpStatus.NOT_FOUND);
    }
}
