package di.gestao.de.utilizadores.controllers;

import di.gestao.de.utilizadores.services.RemoveProfileFromUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RemoveProfileFromUserController {

    @Autowired
    RemoveProfileFromUserService service;

    @PutMapping("/removeProfileFromUser/{username}")
    public ResponseEntity removeProfileFromUser(@PathVariable String username) {
        Boolean res = service.removeProfileFromUser(username);

        if (res)
            return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT);

        return new ResponseEntity<String>("User does not exist", HttpStatus.NOT_FOUND);
    }
}
