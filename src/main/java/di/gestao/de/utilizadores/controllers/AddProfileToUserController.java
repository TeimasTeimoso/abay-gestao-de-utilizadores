package di.gestao.de.utilizadores.controllers;

import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.services.AddProfileToUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AddProfileToUserController {

    @Autowired
    AddProfileToUserService service;

    @PutMapping("/addProfileToUser")
    public ResponseEntity addProfileToUser(@RequestBody User user) {
        Boolean res = service.addProfileTouser(user.getUsername(), user.getProfile());

        if (res)
            return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT);

        return new ResponseEntity<String>("User not found", HttpStatus.NOT_FOUND);
    }

}
