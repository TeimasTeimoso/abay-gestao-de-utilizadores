package di.gestao.de.utilizadores.repositories;

import di.gestao.de.utilizadores.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
    User findByUsername(String username);
    User findByEmail(String email);
}
