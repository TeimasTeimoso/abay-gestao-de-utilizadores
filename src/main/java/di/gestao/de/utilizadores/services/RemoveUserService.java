package di.gestao.de.utilizadores.services;

import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RemoveUserService {

    @Autowired
    UserRepository userRepository;

    public Boolean RemoveUserService(String username) {
        User userToRemove = userRepository.findByUsername(username);

        if (userToRemove != null) {
            userRepository.delete(userToRemove);
            return true;
        }

        return false;
    }
}
