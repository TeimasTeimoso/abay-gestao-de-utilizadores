package di.gestao.de.utilizadores.services;

import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RemoveProfileFromUserService {

    @Autowired
    UserRepository userRepository;

    public Boolean removeProfileFromUser(String username) {
        User user = userRepository.findByUsername(username);

        if (user != null) {
            user.setProfile("basic");
            userRepository.save(user);
            return true;
        }

        return false;
    }

}
