package di.gestao.de.utilizadores.services;

import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SearchUserService {

    @Autowired
    UserRepository userRepository;

    public User getUserInformation(String username) {
        User user = userRepository.findByUsername(username);

        return user;
    }
}
