package di.gestao.de.utilizadores.services;

import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateUserService {

    @Autowired
    UserRepository userRepository;

    public String createUser(User user) {
        if (userRepository.findByUsername(user.getUsername()) != null) {
            return "Username already taken";
        }
        else if (userRepository.findByEmail(user.getEmail()) != null) {
            return "Email already exists in database";
        }

        userRepository.save(user);
        return "User created!";
    }
}
