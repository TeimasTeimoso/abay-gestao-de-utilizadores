package di.gestao.de.utilizadores.services;

import di.gestao.de.utilizadores.models.User;
import di.gestao.de.utilizadores.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateUserInfoService {

    @Autowired
    UserRepository userRepository;

    public Boolean updateUserInfo(User user) {
        User queryUser = userRepository.findByUsername(user.getUsername());

        if (queryUser != null) {
            queryUser.updateInfo(user.getEmail(), user.getPassword(), user.getProfile());
            return true;
        }

        return false;
    }

}
