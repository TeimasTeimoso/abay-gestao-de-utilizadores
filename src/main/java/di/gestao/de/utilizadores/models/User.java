package di.gestao.de.utilizadores.models;

import javax.persistence.*;

@Entity
@Table(name="users")
public class User {

    @Id
    private String username;
    private String password;
    private String email;
    private String profile;

    public User() {}

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.profile = "basic";
    }

    public User(String username, String email, String password, String profile) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.profile = profile;
    }

    /* this constructor is helpful when passing the AddProfileToUser object */
    public User(String username, String profile) {
        this.username = username;
        this.profile = profile;
    }

    public void updateInfo(String email, String password, String profile) {
        this.email = email != null ? email : this.email;
        this.password = password != null ? password : this.password;
        this.profile = profile != null ? profile : this.profile;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getEmail() {
        return this.email;
    }

    public String getProfile() {
        return this.profile;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", profile='" + profile + '\'' +
                '}';
    }
}
