FROM openjdk:14

RUN mkdir -p /app

WORKDIR /app

COPY target/abay-users.jar .

ENTRYPOINT [ "java", "-jar", "/app/abay-users.jar" ]

EXPOSE 8080