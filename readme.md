# Abay - Gestão de Utilizadores

Este repositório contem o trabalho desenvolvido para o serviço de _Gestão de Utilizadores_ da plataforma _Abay_, desenvolvida ao longo de semestre na _UC_ de Engenharia de Software.  

Os restantes serviços da plataforma foram desenvolvidos pelos meus colegas, encontrando-se nos seus respositórios de _Gitlab_.

O trabalho desenvolvido foi desenvolvido em 5 fases:
1. Especificação dos cenários.
2. Implementação dos step-definitions.
3. Adição de persitência.
4. Criação da aplicação _conteinerizada_.
5. Implementação do pipeline _CI/CD_.

Ainda que tenha sido desenvolvido em 5 fases distintas, o trabalho foi desenvolvido de forma continua, tendo assim acontecido alterações ao longo do projeto em fases previamente concluídas.

## Endpoints
O serviço contem os seguintes endpoints:
- _/createUser_
- _/removeUser/{username}_
- _/addProfileToUser_
- _/listUsers_
- _/removeProfileFromUser/{username}_
- _/searchUser/{username}_
- _/updateUserInfo_

Tentei adicionar _Swagger-UI_ ao projeto, de forma a que o utilizador conseguisse uma descrição dos endpoints mais detalhada.

De qualquer forma, é possivel encontrar mais detalhe acerca dos enpoints (para cada cenário) na [wiki](https://gitlab.com/TeimasTeimoso/abay-gestao-de-utilizadores/-/wikis/home) do projeto.

## Pipeline

### Estrutura
O pipeline do projeto é bastante simples, sendo constituído apenas por 4 steps:
- Corre os testes.
- Cria o package.
- Dá push da imagem _docker_ para o _docker_hub_.
- Dá deploy da aplicação no servidor da Universidade de Évora.

Os 2 primeiros steps do pipeline podiam ser agregados num só, tendo um step test_and_build, contudo, de forma a ter uma separação clara dos steps foram implementados de forma distinta. 
### Considerações
O pipeline do projeto foi criado de forma a ser parametrizável.  
Isto é, tentou-se ao máximo que as as variáveis de ambiente fossem definidas nas configurações do gitlab, de modo a que fosse possivel fazer alterações ao pipeline sem ter de fazer alterações ao projeto.  

Esta parametrização não só é uma boa prática como é também bastante útil. Por exemplo, caso o pipeline tivesse muitos steps e falhasse no último, se tivessemos as variaveis hardcoded no projeto, seria necessário fazer um novo commit e reiniciar o pipeline.  
Tendo as configurações diretamente no _Gitlab_ é possivel simplesmente alterar as variáveis e reiniciar o step no qual o pipeline falhou.  

Nas variaveis do _Gitlab_ é possivel alterar todas as credenciais da base de dados, _docker hub_, chave privada do _ssh_ e porta do serviço.